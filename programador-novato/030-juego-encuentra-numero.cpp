#include<iostream>
#include<ctime>

int main(){
	int numeroAleatorio, numeroUsuario, numeroIntentos = 0;

	srand(time(NULL));

	std::cout << "\nJuego encuentra el numero\n";

	numeroAleatorio = 1 + rand() % 100;

	do{
		std::cout << "\nIngresa un numero entero entre 1 y 100: ";
		std::cin >> numeroUsuario;

		if(numeroUsuario < numeroAleatorio){
			std::cout << "\nEl numero que escribiste es inferior al generado, intenta de nuevo.\n";
		}else{
			std::cout << "\nEl numero que escribiste es mayor al generado, intenta de nuevo.\n";
		}
		
		numeroIntentos++;
	}while(numeroUsuario != numeroAleatorio);

	std::cout << "\nFelicidades encontraste el numero.\n";
	std::cout << "\nLos intentos que te tomo encontrar el numero generado es: " << numeroIntentos;
		
	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
