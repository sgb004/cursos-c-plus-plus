#include <iostream>
#include <stdlib.h>

int main(){
	char operacion[11], operando1[5], operando2[5];
	float numero1, numero2, suma = 0;

	std::cout << "Bienvenid@\n";
	std::cout << "Este es un ejemplo del uso de la funcion atof, ";
	std::cout << "se pedira que se escriba una suma donde sus operandos sean numeros de cuatro digitos incluyendo el punto decimal.\n\n";

	std::cout << "Escribe una suma, ejemplo: 12.34+45.67: ";
	std::cin.getline(operacion, sizeof(operacion)+1, '\n');

	for(int i=0; i<5; i++){
		operando1[i] = operacion[i];
	}
	operando1[5] = '\0';
	numero1 = atof(operando1);

	for(int i=0; i<11; i++){
		operando2[i] = operacion[i+6];
	}
	operando2[5] = '\0';
	numero2 = atof(operando2);

	suma = numero1 + numero2;

	std::cout << "\nEl resultado de la suma es " << suma << "\n";
	
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();

	//system("pause");

	return 0;
}
