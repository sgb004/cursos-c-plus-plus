#include<iostream>

int main(){
	int numero, suma = 0, numeroAnterior = 1, numeroGuardado;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo del uso de bucles usando la serie fibonacci.\n\n";

	do{
		std::cout << "Ingresa un numero entero mayor a 1: ";
		std::cin >> numero;
	}while(numero <= 1);

	std::cout << "0,";
	for(int i=0; i<=numero; i++){
		numeroGuardado = suma;
		suma = numeroAnterior + suma;
		numeroAnterior = numeroGuardado;
		std::cout << suma;
		if(i < numero){
			std::cout << ",";
		}
	}

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
