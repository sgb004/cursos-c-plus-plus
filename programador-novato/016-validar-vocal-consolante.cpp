#include<iostream>

int main(){
	char letra;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este ejemplo se validara si una letra ingresada es una vocal o una consonante, si la letra es una consonante, se volvera a solicitar que se ingrese una vocal.\n";

	ingresar:
	std::cout << "\nIngresa una vocal: ";
	std::cin >> letra;

	switch(letra){
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
		case 'A':
		case 'E':
		case 'I':
		case 'O':
		case 'U':
			std::cout << "La letra ingresada es una vocal.\n";
			break;
		default:
			std::cout << "La letra ingresada es una consonante, por favor intenta de nuevo.\n";
			goto ingresar;
			break;
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
