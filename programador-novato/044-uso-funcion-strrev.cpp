#include <iostream>
#include <string.h>

int main(){
	char texto[40], textoRev[40];
	int comparacion;
	

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo del uso de la funcion strrev  ";
	std::cout << "Se solicitara que se ingrese una palabra y se comprobara si es un palindromo.";

	do{
		std::cout << "Ingresa una palabra que sea un palindromo: ";
		std::cin.getline(texto, sizeof(texto)+1, '\n');

		strcpy(textoRev, texto);
		strrev(textoRev);

		comparacion = strcmp(texto, textoRev);

		if(comparacion != 0){
			std::cout << "La palabra ingresada no es un palindromo, por favor, intenta de nuevo.\n";
		}
	}while(comparacion != 0);

	std::cout << "Bien, la palabra ingresada es un palindromo.";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
