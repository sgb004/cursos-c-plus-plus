#include<iostream>

struct Direccion{
	char pais[65];
	char estado[65];
	char municipio[65];
	int calle;
};

struct Empleado{
	char id[6];
	char nombre[65];
	struct Direccion dirEmpleado;
}
e1 = {
	"54555",
	"Eugenio", 
	"México",
	"Estado de México",
	"Atlacomulco",
	12
},
e2 = {
	"50615",
	"Juan",
	"México",
	"Estado de México",
	"Jocotitlan",
	12
};

struct Cliente{
	char nombre[65];
	char telefono[12];
	struct Direccion dirCliente;
} c1, c2;

int main(){

	std::cout << "\nEmpleado 1";
	std::cout << "\n  id: " << e1.id;
	std::cout << "\n  Nombre: " << e1.nombre;
	std::cout << "\n  País: " << e1.dirEmpleado.pais;
	std::cout << "\n  Estado: " << e1.dirEmpleado.estado;
	std::cout << "\n  Municipio: " << e1.dirEmpleado.municipio;
	std::cout << "\n  Calle: " << e1.dirEmpleado.calle;
	
	std::cout << "\n\nEmpleado 2";
	std::cout << "\n  id: " << e2.id;
	std::cout << "\n  Nombre: " << e2.nombre;
	std::cout << "\n  País: " << e2.dirEmpleado.pais;
	std::cout << "\n  Estado: " << e2.dirEmpleado.estado;
	std::cout << "\n  Municipio: " << e2.dirEmpleado.municipio;
	std::cout << "\n  Calle: " << e2.dirEmpleado.calle;

	std::cout << "\n\nCliente 1";
	std::cout << "\n  Nombre: "; std::cin >> c1.nombre;
	std::cout << "  Teléfono: "; std::cin >> c1.telefono;
	std::cout << "  País: "; std::cin >> c1.dirCliente.pais;
	std::cout << "  Estado: "; std::cin >> c1.dirCliente.estado;
	std::cout << "  Municipio: "; std::cin >> c1.dirCliente.municipio;
	std::cout << "  Calle: "; std::cin >> c1.dirCliente.calle;
	
	std::cout << "\nCliente 2";
	std::cout << "\n  Nombre: "; std::cin >> c2.nombre;
	std::cout << "  Teléfono: "; std::cin >> c2.telefono;
	std::cout << "  País: "; std::cin >> c2.dirCliente.pais;
	std::cout << "  Estado: "; std::cin >> c2.dirCliente.estado;
	std::cout << "  Municipio: "; std::cin >> c2.dirCliente.municipio;
	std::cout << "  Calle: "; std::cin >> c2.dirCliente.calle;
	
	std::cout << "\nCliente 1";
	std::cout << "\n  Nombre: " << c1.nombre;
	std::cout << "\n  Teléfono: " << c1.telefono;
	std::cout << "\n  País: " << c1.dirCliente.pais;
	std::cout << "\n  Estado: " << c1.dirCliente.estado;
	std::cout << "\n  Municipio: " << c1.dirCliente.municipio;
	std::cout << "\n  Calle: " << c1.dirCliente.calle;
	
	std::cout << "\n\nCliente 2";
	std::cout << "\n  Nombre: " << c2.nombre;
	std::cout << "\n  Teléfono: " << c2.telefono;
	std::cout << "\n  País: " << c2.dirCliente.pais;
	std::cout << "\n  Estado: " << c2.dirCliente.estado;
	std::cout << "\n  Municipio: " << c2.dirCliente.municipio;
	std::cout << "\n  Calle: " << c2.dirCliente.calle;

	//system("pause");
	std::cout << "\n\nPresiona enter para terminar\n";
	std::cin.get();
	std::getchar();

	return 0;
}
