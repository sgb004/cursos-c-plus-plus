#include<iostream>

int main(){
	int arreglo1[5], arreglo2[5], arreglo3[10];

	std::cout << "\nBienvenid@\n";
	std::cout << "En este ejemplo de arreglos se pediran los valores de dos areglos y luego se uniran dichos arreglos mostrando el resultado de esa union.\n\n";

	for(int i=0; i<5; i++){
		std::cout << "Ingresa el valor del elemento " << (i+1) << " del primer arreglo: ";
		std::cin >> arreglo1[i];
	}

	std::cout << "\n";

	for(int i=0; i<5; i++){
		std::cout << "Ingresa el valor del elemento " << (i+1) << " del segundo arreglo: ";
		std::cin >> arreglo2[i];
	}

	for(int i=0; i<10; i++){
		if(i<5){
			arreglo3[i] = arreglo1[i];
		}else{
			arreglo3[i] = arreglo2[i-5];
		}
	}

	std::cout << "\n";

	for(int i=0; i<10; i++){
		std::cout << "El valor del elemento " << (i+1) << " del tercer arreglo: " << arreglo3[i] << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
