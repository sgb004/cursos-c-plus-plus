#include <iostream>
#include <stdlib.h>

int main(){
	char texto[4], texto1[2], texto2[2];
	int numero1, numero2, suma = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este ejemplo se pedira que se ingrese un numero de 4 digitos, ";
	std::cout << "se tomaran los dos primeros numeros y los dos últimos y se sumaran ambas partes.\n\n";

	std::cout << "Por favor, ingresa 4 numeros, sin ningun tipo de espacio: ";
	std::cin.getline(texto, sizeof(texto)+1, '\n');

	texto1[0] = texto[0];
	texto1[1] = texto[1];
	texto1[2] = '\0';
	numero1 = atoi(texto1);

	texto2[0] = texto[2];
	texto2[1] = texto[3];
	texto2[2] = '\0';
	numero2 = atoi(texto2);

	suma = numero1 + numero2;

	std::cout << "\nLa suma de los dos primeros numeros con los dos ultimos numeros ingresados es: " << suma; 
	
	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();

	//system("pause");
	
	return 0;
}
