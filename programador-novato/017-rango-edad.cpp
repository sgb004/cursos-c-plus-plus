#include<iostream>

int main(){
	int edad;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este programa se validara la edad de un candidat@ que este entre 18 y 40 años.\n";
	std::cout << "Se solicitara un número y se mostrara si esta dentro del rango de edad.\n";

	std::cout << "Ingrese la edad del@ candidat@: ";
	std::cin >> edad;

	if( edad >= 18 && edad <= 40 ){
		std::cout << "\nLa persona que aplica para el puesto es apta para trabajar en la empresa.\n";
	}else{
		std::cout << "\nLa persona que aplica para el puesto no es apta para trabajar en la empresa.\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
