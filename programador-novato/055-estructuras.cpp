#include<iostream>

struct Empleados{
	char nombre[65];
	char direccion[65];
	char telefono[15];
}
e1 = { "Juan", "Argentina", "12345678" },
e2 = { "Maria",  "México", "22334455" },
e3 = { "Pepe", "Chile", "44553322" };

struct Clientes{
	char nombre[65];
	char direccion[65];
}c1, c2;

int main(){
	std::cout << "\nEmpleados\n";
	std::cout << "Nombre e1: " << e1.nombre << ", Dirección: " << e1.direccion << ", Teléfono: " << e1.telefono << "\n";
	std::cout << "Nombre e2: " << e2.nombre << ", Dirección: " << e2.direccion << ", Teléfono: " << e2.telefono << "\n";
	std::cout << "Nombre e3: " << e3.nombre << ", Dirección: " << e3.direccion << ", Teléfono: " << e3.telefono << "\n";

	std::cout << "\nClientes\n";
	std::cout << "Ingresa el nombre del cliente c1: ";
	std::cin >> c1.nombre;
	std::cout << "Ingresa la dirección del cliente c1: ";
	std::cin >> c1.direccion;
	std::cout << "Ingresa el nombre del cliente c2: ";
	std::cin >> c2.nombre;
	std::cout << "Ingresa la dirección del cliente c2: ";
	std::cin >> c2.direccion;

	std::cout << "Nombre del cliente c1: " << c1.nombre << ", Direccion: " << c1.direccion << "\n";
	std::cout << "Nombre del cliente c2: " << c2.nombre << ", Direccion: " << c2.direccion << "\n";

	
	//system("pause");
	std::cout <<  "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();

	return 0;
}
