#include <iostream>

int main(){
	float a, b, r;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de expresiones matematicas y de redondeo de numeros decimales\n";
	std::cout << "Se pediran dos números\n";

	std::cout << "Ingresa el primer numero: ";
	std::cin >> a;

	std::cout << "Ingresa el segundo numero: ";
	std::cin >> b;

	r = ( a / b ) + 1;

	
	std::cout.precision(3);
	std::cout << "\nEl resultado de la operacion es: " << r << "\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	return 0;
}
