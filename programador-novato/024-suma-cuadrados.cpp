#include<iostream>
//#include<math.h>

int main(){
	int suma = 0, numero;
	//float suma = 0, numero;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ciclo for, en este programa se hara la suma de los cuadrados del 1 al 10.\n\n";

	for(int i=1; i<=10; i++){
		numero = i*i;
		//numero = pow(i, 2);
		suma += numero;

		std::cout << "El cuadrado de " << i << " = " << numero << "\n";
	}

	std::cout << "\nLa suma de los cuadrados del 1 al 10 es de " << suma;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	//system("pause");

	return 0;
}
