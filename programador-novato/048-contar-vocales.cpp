#include <iostream>

int main(){
	char texto[30];
	int la = 0, le = 0, li = 0, lo = 0, lu = 0;

	std::cout << "Bienvenid@\n";
	std::cout << "En este programa se contaran las vocales de una frase ingresada.\n\n";

	std::cout << "Escribe una frase o palabra no mayor a 30 caracteres: ";
	std::cin.getline(texto, sizeof(texto)+1, '\n');

	for(int i=0; i<30; i++){
		switch(texto[i]){
			case 'a': la++; break;
			case 'A': la++; break;
			case 'e': le++; break;
			case 'E': le++; break;
			case 'i': li++; break;
			case 'I': li++; break;
			case 'o': lo++; break;
			case 'O': lo++; break;
			case 'u': lu++; break;
			case 'U': lu++; break;
		}
	}

	std::cout << "\nLas vocales en la frase son:\n";
	std::cout << "	Letras a: " << la << "\n";
	std::cout << "	Letras e: " << le << "\n";
	std::cout << "	Letras i: " << li << "\n";
	std::cout << "	Letras o: " << lo << "\n";
	std::cout << "	Letras u: " << lu << "\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	//system("pause");

	return 0;
}
