#include<iostream>
#include<math.h>

int main(){
	int numero;
	float suma = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este programa hara la sumatoria del numero 2 elevado a la i potencia, ";
	std::cout << "donde i sera un numero entero introducido al programa.\n";

	do{
		std::cout << "\nIngresa un nunero entero mayor a uno: ";
		std::cin >> numero;
	}while(numero <= 1);

	for(int i=1; i<=numero; i++){
		suma += pow(2, i);
	}

	std::cout << "\nEl resultado de la sumatoria del nunero 2 elevado a la potencia " << numero << " es: " << suma;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
