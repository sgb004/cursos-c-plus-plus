#include <iostream>

int main(){
	int numeros[5];
	int i, a, p;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ordenamiento por insercion.\n\n";

	std::cout << "Se solicitara que se ingresen 5 numeros.\n";

	for(i=0; i<5; i++){
		std::cout << "Por favor ingresa un numero: ";
		std::cin >> numeros[i];
	}

	for(i=1; i<5; i++){
		a = numeros[i];
		p = i;
		while(p > 0 && numeros[p-1] > a){
			numeros[p] = numeros[p-1];
			p--;
		}
		numeros[p] = a;
	}

	std::cout << "\nLos numeros ingresados ordenados de forma ascendente son:\n";
	for(i=0; i<5; i++){
		std::cout << numeros[i] << "\n";
	}
	
	std::cout << "\nLos numeros ingresados ordenados de forma descendente son:\n";
	for(i=4; i>=0; i--){
		std::cout << numeros[i] << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();

	//system("pause");
	
	return 0;
}
