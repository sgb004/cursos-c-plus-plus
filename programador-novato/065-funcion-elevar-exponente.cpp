#include <iostream>

float numero, elevar;

void pedirDatos();
float retornarExponente(float valor, float exponente);
void muestraExponente(float valor, float );

int main(){
	float resultado;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de uso de funciones.\n\n";

	pedirDatos();
	resultado = retornarExponente(numero, elevar);

	std::cout << "\nEl resultado es: " << resultado;
	muestraExponente(numero, elevar);	

	//system("pause");
	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	return 0;
}

void pedirDatos(){
	std::cout << "Ingresa el numero: ";
	std::cin >> numero;
	std::cout << "Ingresa el exponente: ";
	std::cin >> elevar;	
}

float retornarExponente(float valor, float exponente){
	double resultado = 1;
	for(int i = 0; i < exponente; i++){
		resultado = resultado * valor;
	}
	return resultado;
}

void muestraExponente(float valor, float exponente){
	float resultado = retornarExponente(valor, exponente);
	std::cout << "\nEl resultado es: " << resultado;
}
