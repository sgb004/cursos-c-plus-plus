#include <iostream>

int main(){
	float n1, n2, sum, res, mul, div;

	std::cout << "\nBienvenido.\n";
	std::cout << "Este es un ejemplo de operadores aritmeticos en c++\n";
	std::cout << "Se te pedira que ingreses dos numeros.\n\n";
	
	std::cout << "Ingresa un numero: ";
	std::cin >> n1;
	std::cout << "Ingresa otro numero: ";
	std::cin >> n2;

	sum = n1 + n2;
	res = n1 - n2;
	mul = n1 * n2;
	div = n1 / n2;

	std::cout << "\nEl resultado de la suma es: " << sum << "\n";
	std::cout << "El resultado de la resta es: " << res << "\n";
	std::cout << "El resiltado de la multiplicacion es " << mul << "\n";
	std::cout << "El resultado de la division es: " << div << "\n";

	std::cout<<"\nPresiona una tecla para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	return 0;
}
