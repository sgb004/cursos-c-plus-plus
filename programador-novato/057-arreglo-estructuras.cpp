#include <iostream>

int numEmpleados = 2;
struct Empleados{
	char id[7];
	char nombre[30];
} empleados[2];

int main(){
	bool isNumber = true;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de un arreglo de estructuras.\n";
	std::cout << "Se pedira el nombre y el id de dos empleados, el id del empleado debe ser un numero de 6 digitos\n\n";

	for(int i = 0; i < 2; i++){
		do{
			isNumber = true;

			std::cout << "Escribe el id del empleado " << i + 1 << ": ";
			std::cin >> empleados[i].id;

			for(int j = 0; j < 6; j++){
				int idChar = int(empleados[i].id[j]);
				if(idChar <= 47 || idChar >= 58){
					isNumber = false;
					std::cout << "\nEl id no es valido.\n";
					break;
				}
			}
			empleados[i].id[6] = '\0';
		}while(!isNumber);

		std::cout << "Escribe el nombre del empleado " << i + 1  << ": ";
		std::cin >> empleados[i].nombre;
		std::cout << "\n";
	}
	
	std::cout << "Los empleados ingresados son:\n";
	for(int i = 0; i < 2; i++){
		std::cout << "\nId empleado " << i + 1 << ": " << empleados[i].id;
		std::cout << "\nNombre empleado " << i + 1 << ": " << empleados[i].nombre << "\n";
	}

	//system("pause");
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	return 0;
}
