#include<iostream>

int main(){
	char programador[] = "programador";
	//char novato[] = {'n', 'o', 'v', 'a', 't', 'o'};
	char nombre[5];
	
	std::cout << programador << "\n" << sizeof(programador) << "\n";
	//std::cout << novato << "\n" << sizeof(novato) << "\n";
	
	std::cout << "Escribe tu nombre: ";
	//std::cin >> nombre;
	//gets(nombre);
	std::cin.getline(nombre, sizeof(nombre)+1, '\n');
	std::cout << nombre << "\n";

	std::cout << "Presiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	//system("pause");

	return 0;
}
