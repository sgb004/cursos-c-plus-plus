#include<iostream>

int main(){
	int numeros[100], n, mayor = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de arreglos, el programa devolvera el numero mayor del arreglo.\n\n";

	do{
		std::cout << "Ingresa el numero de elementos entre 1 y 100: ";
		std::cin >> n;
	}while(n < 1 ||  n > 100);

	for(int i=0; i<n; i++){
		std::cout << "Ingresa el valor del elemento " << (i+1) << ": ";
		std::cin >> numeros[i];
		if(numeros[i] > mayor){
			mayor = numeros[i];
		}
	}
/*
	for(int i=0; i<n; i++){
		if(numeros[i] > mayor){
			mayor = numeros[i];
		}
	}
*/
	std::cout << "\n\nEl numero mayor del arreglo es: " << mayor;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
