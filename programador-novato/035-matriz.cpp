#include<iostream>

int main(){
	int matriz[100][100], filas, columnas;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de matrices, se pedira el numero de filas y columas, despues se pedira que se ingresen los valores de esa matriz.\n\n";

	do{
		std::cout << "Escribe el numero de filas debe ser un numero entero entre 1 y 100: ";
		std::cin >> filas;
	}while(filas < 1 || filas > 100);

	std::cout << "\n";

	do{
		std::cout << "Escribe el numero de columnas debe ser un numero entero entre 1 y 100: ";
		std::cin >> columnas;
	}while(columnas < 1 || columnas > 100);

	std::cout << "\n";

	for(int i=0; i<filas; i++){
		for(int j=0; j<columnas; j++){
			std::cout << "Escribe el numero de la fila " << i+1 << " de la columna " << j+1 << ": ";
			std::cin >> matriz[i][j];
		}
		std::cout << "\n";
	}

	for(int i=0; i<filas; i++){
		for(int j=0; j<columnas; j++){
			std::cout << "El valor de la fila " << i+1 << " de la columna " << j+1 << " es " << matriz[i][j] << "\n";
		}
	}

	std::cout << "\n";

	for(int i=0; i<filas; i++){
		for(int j=0; j<columnas; j++){
			std::cout << matriz[i][j] << " ";
		}
		std::cout << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
