#include<iostream>

int main(){
	int numeros[] = {1, 2, 3, 4, 5}, suma = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este un ejemplo de arreglos.\n";
	std::cout << "Se hara la suma de un arreglo\n";

	for(int i=0; i<5; i++){
		std::cout << "\nindex: " << i << ", numero: " << numeros[i];
		suma += numeros[i];
	}

	std::cout << "\n\nEl resultado de la suma es: " << suma;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	//system("pause");	

	return 0;
}
