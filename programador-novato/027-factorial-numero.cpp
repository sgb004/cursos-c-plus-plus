#include<iostream>

int main(){
	int numero, factorial = 1;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este programa obtiene el factorial de un numero.\n";

	do{
		std::cout << "\nIngresa un nunero entero mayor a uno: ";
		std::cin >> numero;
		
	}while(numero <= 1);

	for(int i=1; i<=numero; i++){
		factorial *= i;
	}

	std::cout << "\nEl factorial del numero " << numero << " es " << factorial;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
