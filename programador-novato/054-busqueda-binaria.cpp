#include<iostream>

int main(){
	std::cout << "\n";
	int numLen = 17;
	int numeros[numLen] = { 1, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59 };
	int input, start, end, middle;
	bool found = false;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de busqueda binaria.\n";

	do{
		std::cout << "\nIngresa un numero: ";
		std::cin >> input;
		start = 0;
		end = numLen;
		
		while( start <= end ){
			middle = (end + start) / 2;
			
			if( numeros[middle] == input ){
				std::cout << "\nEl numero ingresado fue encontrado.\n";
				found = true;
				break;
			}else if(middle == start){
				std::cout << "\nEl numero ingresado no fue encontrado, por favor, vuelve a intentarlo.\n";
				break;
			}

			if( numeros[middle] > input){
				end = middle;
				middle = (end + start) / 2;
			}

			if( numeros[middle] < input){
				start = middle;
			}
		}
	}while(!found);

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();

	//system("pause");

	return 0;
}
