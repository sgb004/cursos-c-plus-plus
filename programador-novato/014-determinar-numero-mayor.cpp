#include<iostream>

int main(){
	int numero1, numero2;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este programa se solicitaran dos numeros y se determinara cual de ellos es el mayor.\n";

	std::cout << "Escribe el numero 1: ";
	std::cin >> numero1;

	std::cout << "Escribe el numero 2: ";
	std::cin >> numero2;

	if( numero1 == numero2 ){
		std::cout << "\nLos numeros son iguales.\n";
	}else if( numero1 > numero2 ){
		std::cout << "\nEl numero 1 es mayor.\n";
	}else{
		std::cout << "\nEl numero 2 es mayor.\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
