#include<iostream>

int main(){
	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ciclo for\n\n";

	for(int i=1; i<=10; i++){
		std::cout << i << "\n";
	}

	std::cout << "\n";

	for(int i=10; i>=0; i--){
		std::cout << i << "\n";
	}

	std::cout << "\n";

	std::cout << "\n";
	for(int i=10; i>=0; i-=2){
		std::cout << i << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	//system("pause");

	return 0;
}
