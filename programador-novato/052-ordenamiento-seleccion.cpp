#include <iostream>

int main(){
	int numeros[5];
	int i, j, p, n;

	std::cout << "\nBinvenid@\n";
	std::cout << "\nEste es un ejemplo de ordenamiento por seleccion.\n\n";

	std::cout << "Se solicitara que se ingresen 5 numeros.\n";

	for(i=0; i<5; i++){
		std::cout << "Por favor ingresa un numero: ";
		std::cin >> numeros[i];
	}

	for(i=0; i<5; i++){
		p = i;
	
		for(j=i+1; j<5; j++){
			if(numeros[p] > numeros[j]){
				p = j;
			}
		}

		n = numeros[i];
		numeros[i] = numeros[p];
		numeros[p] = n;
	}

	std::cout << "\nLos numeros ingresados ordenados de forma ascendente son:\n";
	for(i=0; i<5; i++){
		std::cout << numeros[i] << "\n";
	}

	std::cout << "\nLos numeros ingresados ordenados de forma descendente son:\n";
	for(i=4; i>=0; i--){
		std::cout << numeros[i] << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	//system("pause");

	return 0;
}
