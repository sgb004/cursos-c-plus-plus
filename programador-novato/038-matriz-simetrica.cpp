#include<iostream>

int main(){
	int filMatrizA, colMatrizA, filMatrizB, colMatrizB;
	bool esSimetrica;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este ese un ejemplo de matrices, en este ejemplo se comprobara si una matriz es simetrica.\n";

	do{
		esSimetrica = true;
	
		std::cout << "\nIngresa las medidas de la matriz A.\n";
		std::cout << "Deben ser numeros enteros separados por un espacio: ";
		std::cin >> filMatrizA >> colMatrizA;

		std::cout << "\nIngresa las medidas de la matriz B.\n";
		std::cout << "Deben ser numeros enteros separados por un espacio: ";
		std::cin >> filMatrizB >> colMatrizB;

		if(filMatrizA != colMatrizA || filMatrizB != colMatrizB || filMatrizA != filMatrizB){
			std::cout << "\nLas matrices simetricas tienen el mismo numero de filas y columnas, por favor, ingresa correctamente el numero de filas y columnas.\n";
			esSimetrica = false;
		}
	}while(!esSimetrica);

	int matrizA[filMatrizA][colMatrizA], matrizB[filMatrizB][colMatrizB];

	do{
		esSimetrica = true;
	
		std::cout << "\nEscribe los valores de la matriz A:\n";

		for(int i=0; i<filMatrizA; i++){
			for(int j=0; j<colMatrizA; j++){
				std::cout << "[" << (i+1) << "][" << (j+1) << "] = ";
				std::cin >> matrizA[i][j];
			}
		}

		std::cout << "\nEscribe los valores de la matriz B:\n";

		for(int i=0; i<filMatrizB; i++){
			for(int j=0; j<colMatrizB; j++){
				std::cout << "[" << (i+1) << "][" << (j+1) << "] = ";
				std::cin >> matrizB[i][j];
			}
		}

		for(int i=0; i<filMatrizA; i++){
			for(int j=0; j<colMatrizA; j++){
				if(matrizA[i][j] != matrizB[j][i]){
					esSimetrica = false;
					break;
				}
			}
			if(!esSimetrica){
				std::cout << "\nLa matriz no es simetrica, prueba de nuevo.\n";
				break;
			}
		}
	}while(!esSimetrica);

	std::cout << "\nLas matrices son simetricas\n";
	
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
