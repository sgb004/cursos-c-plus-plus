#include <iostream>

int dato1, dato2;

void pideDatos();
int retornaMultiplicacion(int a, int b);
void muestraMultiplicacion(int a, int b);

int main(){
	int resultado;

	std::cout << "\nBienvenid@\n";
	std::cout << "\nEste es un ejemplo de uso de funciones.\n\n";
	
	pideDatos();
	resultado = retornaMultiplicacion(dato1, dato2);
	std::cout << "El resultado de la multiplicación es: " << resultado;
	muestraMultiplicacion(dato1, dato2);

	//system("pause");
	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	return 0;
}

void pideDatos(){
	std::cout << "Ingresa dos numeros enteros positivos:\n";
	do{
		std::cout << "Dato 1: ";
		std::cin >> dato1;
		if(dato1 < 0){
			std::cout << "Error: No es un numero positivo.\n";
		}
	}while(dato1 < 0);

	do{
		std::cout << "Dato 2: ";
	    std::cin >> dato2;
	    if(dato1 < 0){
	    	std::cout << "Error: No es un numero positivo.\n";
	    }
	}while(dato2 < 0);
}

int retornaMultiplicacion(int a, int b){
	return a * b;
}

void muestraMultiplicacion(int a, int b){
	std::cout << "\nEl resultado de la multiplicación es: " << a * b << " (Desde una función)";
}
