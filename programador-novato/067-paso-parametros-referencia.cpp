#include <iostream>

void cambiaValor(int& val1, int& val2);

int main(){
	int num1 = 0;
	int num2 = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de paso de parametros por referencia.\n";

	std::cout << "Ingresa el valor de num1: ";
	std::cin >> num1;

	std::cout << "Ingresa el valor de num2: ";
	std::cin >> num2;

	std::cout << "\nNUMEROS INGRESADOS\n";
	std::cout << "num1: " << num1 << "\n";
	std::cout << "num2: " << num2 << "\n";

	cambiaValor(num1, num2);

	std::cout << "\nNUMEROS CAMBIADOS\n";
	std::cout << "num1: " << num1 << "\n";
	std::cout << "num2: " << num2 << "\n";

	//system("pause");
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	return 0;
}

void cambiaValor(int& val1, int& val2){
	val1 = 10;
	val2 = 20;
}
