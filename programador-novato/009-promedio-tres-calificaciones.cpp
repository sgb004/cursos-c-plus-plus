#include<iostream>

int main(){
	float practicas, participacion, examen, promedio;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este ejemplo se calcula el promedio de 3 calificaciones donde:\n";
	std::cout << "La calificación de las practicas corresponde al 40% del total de la calificacion.\n";
	std::cout << "La calificación de la participación corresponde al 20% del total de la calificacion.\n";
	std::cout << "La calificación del examen corresponde al 40% del total de la calificacion.\n";
	std::cout << "Se solicitarán las 3 calificaciones.\n\n";

	std::cout << "Ingresa la calificacion de las practicas: ";
	std::cin >> practicas;

	std::cout << "Ingresa la calificacion de la participación: ";
	std::cin >> participacion;

	std::cout << "Ingresa la calificacion del examen: ";
	std::cin >> examen;

	//practicas = practicas * 0.4;
	practicas *= 0.4;
	//participacion = participacion * 0.2;
	participacion *= 0.2;
	//examen = examen * 0.4;
	examen *= 0.4;

	promedio = practicas + participacion + examen;

	std::cout.precision(3);
	std::cout << "\nLa calificacion final es de: " << promedio << "\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
