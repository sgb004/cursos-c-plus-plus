#include<iostream>

int main(){
	int numero, suma = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ciclo for.\n";
	std::cout << "Este programa hara la suma de n numeros consecutivos, donde n sera un numero entero ingresado al programa.\n\n";

	do{
		std::cout << "Ingresa un numero entero mayor a uno: ";
		std::cin >> numero;
	}while(numero <= 1);

	for(int i=1; i<=numero; i++){
		suma += i;
	}

	std::cout << "\nLa sumatoria del numero " << numero << " es " << suma;

	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
