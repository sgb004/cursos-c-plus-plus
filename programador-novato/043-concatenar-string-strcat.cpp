#include <iostream>
#include <string.h>

int main(){
	char nombre[20], apellido[20], nombreApellido[40] = {};

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de concatenacion de dos strings, se solicitara el nombre y apellido del usuari@.\n\n";

	std::cout << "Ingresa tu nombre: ";
	std::cin.getline(nombre, sizeof(nombre)+1, '\n');

	std::cout << "Ingresa tu apellido: ";
	std::cin.getline(apellido, sizeof(apellido)+1, '\n');

	strcat(nombreApellido, nombre);
	strcat(nombreApellido, " ");
	strcat(nombreApellido, apellido);

	std::cout << "\nTu nombre y apellido son: " << nombreApellido;
	
	std::cout << "\n\nPresiona enter para terminar...\n";
	//std::cin.get();
	std::getchar();

	//system("pause");

	return 0;
}
