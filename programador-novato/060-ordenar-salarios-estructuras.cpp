#include <iostream>
#include <string.h>

struct Empleados{
	char nombre[30];
	int salario;
} empleados[3] = {
	{"Juana", 0},
	{"Maria", 0},
	{"Pepe", 0}
};

int main(){
	char nombre[30];
	int salario;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ordrnamiento con estructuras\n";

	std::cout << "\nIngresa el salario de los empleados:\n";
	for(int i = 0; i < 3; i++){
		std::cout << "Salario del/a emplead@ " << empleados[i].nombre << ": ";
		std::cin >> empleados[i].salario;
	}

	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 2; j++){
			if(i == j) continue;

			if(empleados[i].salario < empleados[j].salario){
				strcpy(nombre, empleados[i].nombre);
				salario = empleados[i].salario;

				strcpy(empleados[i].nombre, empleados[j].nombre);
				empleados[i].salario = empleados[j].salario;

				strcpy(empleados[j].nombre, nombre);
				empleados[j].salario = salario;
			}
		}
	}

	std::cout << "\nSalarios ordenados de menor a mayor:\n";
	for(int i = 0; i < 3; i++){
		std::cout << "La/el emplead@ " << empleados[i].nombre << " gana: " << empleados[i].salario << "\n";
	}

	//system("pause");
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	return 0;
}
