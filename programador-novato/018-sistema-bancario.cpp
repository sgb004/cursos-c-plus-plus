#include<iostream>

int main(){
	int opcion;
	float total = 1000.00, ingreso, egreso;

	std::cout << "\nBienvenid@ al sistema bancario.\n";

	menu:
	std::cout << "\nSelecciona una opción y presiona enter:\n";
	std::cout << "1. Consultar saldo.\n";
	std::cout << "2. Ingresar dinero.\n";
	std::cout << "3. Retirar dinero.\n";
	std::cout << "4. Salir\n\n";
	std::cin >> opcion;

	//std::cout.precision(3);

	switch(opcion){
		case 1:
			std::cout << "\nTu saldo disponible es de: $" << total << "\n";
			goto menu;
		case 2:
			std::cout << "\nIngresa la cantidad que quieras depositar: $";
			std::cin >> ingreso;
			total += ingreso;
			std::cout << "Tu saldo disponible es de: $" << total << "\n";
			goto menu;
		case 3:
			std::cout << "\nIngresa la cantidad que quieras retirar: $";
			std::cin >> egreso;
			if( egreso > total ){
				std::cout << "No tienes disponible la cantidad que quieres retirar, tu saldo es de: $" << total << "\n";
			}else{
				total -= egreso;
				std::cout << "Te queda disponible: $" << total << "\n";
			}
			goto menu;
		case 4:
			break;
		default:
			std::cout << "\nLa opcion seleccionada no esta disponible.\n";
			goto menu;
	}

	return 0;
}
