#include <iostream>

int main(){
	char sexo[10];
	int edad;
	float estatura;

	std::cout << "\nBienvenid@.\n";
	std::cout << "Este es un ejemplo de uso de tipo de dato char.\n";
	std::cout << "En este ejemplo se pedira que se ingrese el sexo, edad y estatura.";

	std::cout << "\nSexo: ";
	std::cin >> sexo;

	std::cout << "Edad: ";
	std::cin >> edad;

	std::cout << "Estatura: ";
	std::cin >> estatura;

	std::cout << "\nLos datos registrados son: ";
	std::cout << "\nSexo: " << sexo << "\n";
	std::cout << "Edad: " << edad << "\n";
	std::cout << "Estatura: " << estatura << "\n";

	std::cout << "\nPresiona una tecla para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	return 0;
}
