#include <iostream>

int suma(int a, int b);

int main(){
	int a, b, r;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este programa es un ejemplo de uso de funciones.\n";

	std::cout << "\nSe solicitaran 2 valores y se devolvera la suma de ambos.\n";

	std::cout << "Ingresa el valor de a: ";
	std::cin >> a;

	std::cout << "Ingresa el valor de b: ";
	std::cin >> b;
	
	r = suma(a, b);
	std::cout << "El resultado es: " << r;

	//system("pause");
	std::cout << "\n\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();

	return 0;
}

int suma(int a, int b){
	return a + b;
}
