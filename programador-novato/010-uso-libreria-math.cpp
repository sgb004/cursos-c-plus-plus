#include<iostream>
#include<math.h>

int main(){
	float x, y, r;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de uso de la libreria math.\n";
	std::cout << "Revisa la imagen 010-uso-libreria-math.png para ver la expresion matematica que resolvera este programa.\n";
	std::cout << "A continuacion se solicitaran 2 numeros.\n\n";

	std::cout << "Ingresa el valor de x: ";
	std::cin >> x;

	std::cout << "Ingresa el valor de y: ";
	std::cin >> y;

	r = (sqrt(x)) / (pow(y,2) - 1);

	std::cout << "\nEl resultado de la operacion es: " << r << "\n";

	std::cout << "\nPresiona enter para terminar...";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
