#include<iostream>

int main(){
	int i = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Ejemplo de uso de while:\n\n";

	while(i <= 10){
		std::cout << i << "\n";
		i++;
	}

	i--;
	std::cout << "\n";

	while(i >= 0){
		std::cout << i << "\n";
		i--;
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	//system("pause");
	return 0;
}
