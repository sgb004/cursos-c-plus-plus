#include<iostream>

int main(){
	int numeroIngresado, numeroAencontrar = 5;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de condicional if - else.\n\n";

	std::cout << "Intenta adiviar el numero.\n";
	std::cout << "Escribe un numero: ";
	std::cin >> numeroIngresado;

	if(numeroIngresado == numeroAencontrar){
		std::cout << "\n¡Felicidades!, Adivinaste el numero.\n";
	}else{
		std::cout << "\nLo siento, ese no era el numero.\n";

		if(numeroIngresado >= numeroAencontrar){
			std::cout << "El numero que ingresaste es mayor al numero a adivinar.\n";
		}else if(numeroIngresado <= numeroAencontrar){
			std::cout << "El numero que ingresaste es menor al numero a adivinar.\n";
		}
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
