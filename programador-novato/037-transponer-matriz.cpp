#include<iostream>

int main(){
	int matrizA[3][3], matrizB[3][3];

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de transponer una matriz, se pedira que se ingresen los valores de una matriz de 3x3.\n\n";

	std::cout << "Escribe los valores de la matriz: \n";

	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			std::cout << "[" << (i+1) << "][" << (j+1) << "] = ";
			std::cin >> matrizA[i][j];
		}
	}

	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			matrizB[j][i] = matrizA[i][j];
		}
	}

	std::cout << "\nLos valores de la matriz son: \n";
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			std::cout << "[" << matrizA[i][j] << "]";
		}
		std::cout << "\n";
	}


	std::cout << "\nLa matriz transpuesta es: \n";
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			std::cout << "[" << matrizB[i][j] << "]";
		}
		std::cout << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	
	return 0;
}
