#include<iostream>

int main(){
	int numeros[100], n;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este ejemplo de arreglos se solicitara que se ingrese la cantidad de elementos de un arreglo y posteriormente se solicitara que se ingrese el valor de cada elemento.\n\n";

	do{
		std::cout << "Ingresa la cantidad de elementos del 1 al 100: ";
		std::cin >> n;
	}while(n < 1 || n > 100);

	for(int i=0; i<n; i++){
		std::cout << "Ingresa el valor del elemento " << (i+1) << ": ";
		std::cin >> numeros[i];
	}

	std::cout << "\nLos valores del arreglo son: \n";

	for(int i=0; i<n; i++){
		std::cout << "Elemento: " << (i+1) << " = " << numeros[i] << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
