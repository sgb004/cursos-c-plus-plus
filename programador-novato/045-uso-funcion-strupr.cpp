#include <iostream>
#include <cctype>
#include <cstring>

char *strupr(char *str){
	int l = strlen(str);
	for(int i=0; i<l; i++){
		str[i] = toupper(str[i]);
	}
	return str;
}

int main(){
	char texto1[40], texto2[40];
	int c;

	std::cout << "Bienvenid@\n";
	std::cout << "Este es un ejemplo de uso de la funcion strupr, ";
	std::cout << "se solicitara que se ingrese un texto  en minusculas o mayusculas ";
	std::cout << "y luego que ingrese un texto similar.\n\n";

	do{
		std::cout << "Por favor, ingresa un texto: ";
		std::cin.getline(texto1, sizeof(texto1)+1, '\n');

		std::cout << "Por favor, ingresa un texto similar (sin tomar en cuenta minusculas o mayusculas): ";
		std::cin.getline(texto2, sizeof(texto2)+1, '\n');

		strupr(texto1);
		strupr(texto2);

		c = strcmp(texto1, texto2);

		if(c != 0){
			std::cout << "\nLos textos " << texto1 << " y " << texto2 << " no son similares, por favor, prueba de nuevo.\n\n";
		}
	}while( c != 0 );

	std::cout << "\nBien, los dos textos son similares.";
	
	std::cout << "\n\nPresiona enter para terminar...";
	//std::cin.get();
	std::getchar();

	//system("pause");

	return 0;
}
