#include <iostream>

int main(){
	char letra;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de uso de do while\n\n";

	do{
		std::cout << "Por favor escribe la letra a: ";
		std::cin >> letra;
	}while(letra == 'a');

	std::cout << "\nFin del programa, escribiste una letra diferente a la a\n\n";
	std::cin.get();
	//std::getchar();
	//system("pause");

	return 0;
}
