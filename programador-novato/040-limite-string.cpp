#include <iostream>
#include <string.h>

int main(){
	char texto[100];
	int longitud = 0;

	do{
		std::cout << "Por favor ingresa un texto con un minimo de 10 letras: ";
		std::cin.getline(texto, sizeof(texto)+1, '\n');

		longitud = strlen(texto);
	}while(longitud < 10);

	std::cout << "Presiona enter para terminar...\n";
	//std::cin.get();
	std::getchar();

	//system("pause");
	
	return 0;
}
