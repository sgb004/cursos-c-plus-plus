/**
 * Programa que pida numeros enteros y los vaya sumando.
 * Si el numero introducido esta dentro de 100 y 200 o es 0 cerrar el programa
 */

#include<iostream>

int main(){
	int numero, suma = 0;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de uso de do while.\n";
	std::cout << "El programa solicitara un numero entero, si este se encuentra entre 100 y 200 o si es 0 se terminará el programa.\n";

	do{
		std::cout << "\nEscribe un numero entero: ";
		std::cin >> numero;

		suma += numero;
		std::cout << "El resultado de la sumatoria es: " << suma << "\n";
	}while(numero != 0 && (numero < 100 || numero > 200));

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
