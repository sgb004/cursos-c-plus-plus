#include <iostream>

int main(){
	float a, b, c, d, e, f, r;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es otro ejemplo de expresiones matematicas.\n";
	std::cout << "Se pediran que se ingresen 6 números para realizar la operacion matematica:\n";
	std::cout << "( a + ( b / c ) ) / ( d + ( e / f ) )\n\n";
	
	std::cout << "Valor de a: ";
	std::cin >> a;

	std::cout << "Valor de b: ";
	std::cin >> b;

	std::cout << "Valor de c: ";
	std::cin >> c;

	std::cout << "Valor de d: ";
	std::cin >> d;

	std::cout << "Valor de e: ";
	std::cin >> e;

	std::cout << "Valor de f: ";
	std::cin >> f;

	r = ( a + ( b / c ) ) / ( d + ( e / f ) );

	std::cout << "\nEl resultado de la operacion es: " << r << "\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");
	return 0;
}
