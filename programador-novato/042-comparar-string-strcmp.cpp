#include <iostream>
#include <string.h>

int main(){
	char texto1[] = "ayer";
	//char texto2[] = "viernes";
	//char texto2[] = "aber";
	char texto2[] = "AYER";
	int comparacion = strcmp(texto1, texto2);

	std::cout << "Bienvenid@\n";
	std::cout << "Este es un ejemplo de comparacion de string usando la funcion strcmp.\n\n";

	if( comparacion == 0 ){
		std::cout << "Ambos textos son iguales.";
	}else{
		std::cout << "Ambos textos son diferentes.\n";

		if( comparacion > 0 ){
			std::cout << "El texto 1 es mayor al texto 2.\n";
		}else{
			std::cout << "El texto 1 es menor al texto 2.\n";
		}
	}


	std::cout << "\nPresiona enter para terminar...";
	//std::cin.get();
	std::getchar();

	//system("pause");

	return 0;
}
