/*
 * Este programa pedira que se digite un numero entre 1 y 10, validara que ese numero este dentro del rango entre 1 y 10.
 * Mostrara los multiplos del numero del 1 al 10.
 */

#include<iostream>

int main(){
	int numero;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este programa pedira que se escriba un numero entre 1 y 10, ";
	std::cout << "y mostrara los multiplos del numero en multiplos de 1 al 10.\n";
	
	do{
		std::cout << "\nIngresa un numero entre el 1 y el 10: ";
		std::cin >> numero;
	}while(numero < 1 || numero > 10);

	std::cout << "\n";

	for(int i=1; i<11; i++){
		std::cout << i << " x " << numero << " = " << (i * numero) << "\n";
	}

	std::cout << "\nPresiona enter pars terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
