#include <iostream>
#include <ctime>

struct Empleados{
	char nombre[50];
	int anioIngreso;
	int aniosTrabajados;
	int diasVacaciones;
} empleados[3] = {{"Pepe"}, {"Maria"}, {"Juana"}};

int main(){
	time_t t = time(NULL);
	tm* timerPtr = localtime(&t);
	int anioActual = timerPtr->tm_year + 1900;

	std::cout << "\nBienvenid@\n";
	std::cout << "\nIngresa los años de ingreso de cada empleado.\n";
	
	for(int i = 0; i < 3; i++){
		std::cout << "\nAño de ingreso de " << empleados[i].nombre << ": ";
		std::cin >> empleados[i].anioIngreso;
		empleados[i].aniosTrabajados = anioActual - empleados[i].anioIngreso;

		switch(empleados[i].aniosTrabajados){
			case 0 ... 3: empleados[i].diasVacaciones = 5; break;
			case 4 ... 10: empleados[i].diasVacaciones = 10; break;
			case 11 ... 100: empleados[i].diasVacaciones = 15; break;
		}
	}

	std::cout << "\nDias de vacaciones por empleado\n";
	for(int i = 0; i < 3; i++){
		std::cout << "Al empleado " << empleados[i].nombre << " le corresponden " << empleados[i].diasVacaciones << " por sus " << empleados[i].aniosTrabajados << " años trabajados.\n";
	}

	//system("pause");
    std::cout << "\nPresiona enter para terminar...\n";
    std::cin.get();
    std::getchar();

	return 0;
}


