#include <iostream>

struct Materias{
	char nombre[50];
	float calificacion;
} materias[6] = {
	{"Fisica",			 0},
	{"Quimica",			 0},
	{"Sociales",		 0},
	{"Geografia",		 0},
	{"Matematicas",		 0},
	{"Programacion",	 0},
};

int main(){
	bool isValid = true;
	float suma = 0, promedio;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de arreglo de estructuras.\n";
	std::cout << "\nSe solicitaran las calificaciones de 6 materias.\n";

	for(int i = 0; i < 6; i++){
		do{
			isValid = true;
			std::cout << "\nEscribe la calificacion de " << materias[i].nombre << ": ";
			std::cin >> materias[i].calificacion;

			if(materias[i].calificacion < 0 || materias[i].calificacion > 10){
				isValid = false;
				std::cout << "La calificacion debe ser mayor a 0 y menor a 10.\n";
			}
		}while(!isValid);

		suma += materias[i].calificacion;
	}

	promedio = suma / 6;

	std::cout << "\nLa calificacion final es de: " << promedio << "\n";

	if(promedio < 6){
		std::cout << "\n¡¡¡Reprobado!!!\n";
	}else if(promedio >= 6 && promedio <= 6.9){
		std::cout << "\nPasaste\n";
	}else if(promedio >= 7 && promedio <= 8.9){
		std::cout << "\nBien hecho\n";
	}else if(promedio >= 9 && promedio <= 10){
		std::cout << "\n¡¡¡Excelente!!!\n";
	}

	//system("pause");
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	return 0;
}
