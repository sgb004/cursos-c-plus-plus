#include <iostream>

int main(){
	int list[5] = {1, 2, 3, 4, 5}, n, i;
	bool found = false;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de busqueda secuencial.\n";

	do{
		std::cout << "\nIngresa un numero: ";
		std::cin >> n;
		for(i=0; i<5; i++){
			if(n == list[i]){
				found = true;
				break;
			}
		}
		if(!found){
			std::cout << "El numero ingresado no fue encontrado, por favor, vuelve a intentarlo.\n";
		}
	}while(!found);

	std::cout << "\nEl numero ingresado fue encontrado.\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	//system("pause");

	return 0;
}
