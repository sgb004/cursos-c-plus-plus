#include <iostream>

struct Clientes{
	char nombre[30];
	int saldo;
} clientes[3] = {
	{"Pepe", 0},
	{"Maria", 0},
	{"Juana", 0}
};

void agregarSaldo(int id){
	int saldo;

	std::cout << "\nIngresa el saldo del ciente " << clientes[id].nombre << ": ";
	std::cin >> saldo;

	clientes[id].saldo += saldo;

	std::cout << "\nAhora el saldo del ciente " << clientes[id].nombre << " es de: " << clientes[id].saldo;
}

int main(){
	int id;
	bool isValid = false;
	char option = 'o';

	std::cout << "\nBienvenid@\n";
	std::cout << "Este programa sirve para recargar tiempo aire.\n";
	std::cout << "\nSe muestra a continuación la lista de clientes:\n";

	for(int i = 0; i < 3; i++){
		std::cout << "Id: " << i << " | Nombre: " << clientes[i].nombre << "\n";
	}

	while(option == 'o' || option == 'm' || option == 'c'){
		if(option == 'o'){
			do{
				std::cout << "\nSelecciona el id del cliente al que deseas agregar saldo: ";
				std::cin >> id;

				isValid = !(clientes[id].nombre[0] == '\0');

				if(!isValid){
					std::cout << "\nEl cliente no existe.\n";
				}
			}while (!isValid);
			agregarSaldo(id);
		}else if(option == 'm'){
			agregarSaldo(id);
		}else{
			std::cout << "\nTiempo aire de los clientes:";
			for(int i = 0; i < 3; i++){
				std::cout << "\nId: " << i << " | Nombre: " << clientes[i].nombre << " | Tiempo aire: " << clientes[i].saldo;
			}
		}
		
		std::cout << "\n\nPara continuar selecciona una opción:";
		std::cout << "\n\tIngresar mas tiempo aire al usuario " << clientes[id].nombre << " presiona 'm'";
		std::cout << "\n\tIngresar tiempo aire a otro usuario presiona 'o'";
		std::cout << "\n\tVer la lista de clientes 'c'";
		std::cout << "\n\tSalir, presiona cualquier otra tecla.\n\t";
		std::cin >> option;
	}
	
	
	//system("pause");
	std::cout << "\n";
	/*
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	*/
	
	return 0;
}
