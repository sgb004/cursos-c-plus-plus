#include<iostream>

int main(){
	int numero;

	std::cout << "\nBienvenid@\n";
	std::cout << "En este programa se validará si un número es par o impar.\n";
	std::cout << "\nIngresa un numero: ";
	std::cin >> numero;

	if( numero % 2 == 0 ){
		std::cout << "\nEl numero " << numero << " es par.\n";
	}else{
		std::cout << "\nEl numero " << numero << " es impar.\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
