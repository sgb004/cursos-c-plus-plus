#include <iostream>

template <class elTipo>

void mostrarNumero(elTipo numero);

int main(){
	int numeroEntero = 5;
	float numeroFloat = 5.5656;
	double numeroDoble = 54.545454;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de templates.\n\n";

	mostrarNumero(numeroEntero);
	mostrarNumero(numeroFloat);
	mostrarNumero(numeroDoble);

	//system("pause");
	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	//std::getchar();
	
	return 0;
}

template <class elTipo>

void mostrarNumero(elTipo numero){
	std::cout << "Mostrar numero de cualquier tipo: " << numero << "\n";
}
