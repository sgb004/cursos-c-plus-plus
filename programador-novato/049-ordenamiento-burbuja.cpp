#include <iostream>

int main(){
	int numeros[5];
	int i, j, n;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de ordenamiento burbuja.\n\n";

	std::cout << "Se solicitara que se ingresen 5 numeros.\n";

	for(i=0; i<5; i++){
		std::cout << "Por favor ingresa un numero: ";
		std::cin >> numeros[i];
	}

	for(i=0; i<4; i++){
		for(j=0; j<4; j++){
		if(numeros[j] > numeros[j+1]){		
			n = numeros[j];
			numeros[j] = numeros[j+1];
			numeros[j+1] = n;
		}}
	}

	std::cout << "\nLos numeros ingresados ordenados de forma ascendente son:\n";

	for(i=0; i<5; i++){
		std::cout << numeros[i] << "\n";
	}

	std::cout << "\nLos numeros ingresados ordenados de forma descendente son:\n";
	for(i=4; i>=0; i--){
		std::cout << numeros[i] << "\n";
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	
	//system("pause");

	return 0;
}
