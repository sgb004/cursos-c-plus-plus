#include<iostream>

int main(){
	float a, b, c;

	std::cout << "\nBienvenid@\n";
	std::cout << "Este es un ejemplo de inyercambio de datos.";
	std::cout << "Se solicitatan dos numeros y despues se imprimiran en pantalla intercambiando su posicion.\n\n";
	
	std::cout << "Primer numero: ";
	std::cin >> a;

	std::cout << "Segundo numero: ";
	std::cin >> b;

	c = a;
	a = b;
	b = c;

	std::cout << "\nEl primer numero ahora es: " << a << "\n";
	std::cout << "El segundo numero ahora es: " << b << "\n";

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//sysyem("pause");
	return 0;
}
