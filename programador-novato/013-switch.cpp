#include<iostream>

int main(){
	int menuItem;

	std::cout << "\nBienvenid@\n";
	std::cout << "Ejemplo de uso de switch con un menu.\n";

	menu:
	std::cout << "\nIngresa un numero del 1 al 6 para seleccionar un elemento del menu, ingresa 7 para salir del menu.\n";
	std::cin >> menuItem;
	
	switch(menuItem){
		case 1:
			std::cout << "\nSeleccionaste el uno.\n";
			goto menu;
			break;
			
		case 2:
			std::cout << "\nSeleccionaste el dos.\n";
			goto menu;
			break;

		case 3:    
			std::cout << "\nSeleccionaste el tres.\n";
			goto menu;
			break;

		case 4:
			std::cout << "\nSeleccionaste el cuatro.\n";
			goto menu;
			break;

		case 5:
			std::cout << "\nSeleccionaste el cinco.\n";
			goto menu;
			break;

		case 6:
			std::cout << "\nSeleccionaste el ultimo.\n";
			goto menu;
			break;
			
		default:
			std::cout << "\nSaliste del menu.\n";
		
	}

	std::cout << "\nPresiona enter para terminar...\n";
	std::cin.get();
	std::getchar();
	//system("pause");

	return 0;
}
